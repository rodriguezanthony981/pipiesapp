import { Component } from '@angular/core';
import { interval } from 'rxjs';

@Component({
  selector: 'app-no-comunes',
  templateUrl: './no-comunes.component.html',
  styles: [
  ]
})
export class NoComunesComponent {
  //i18nSelect
  nombre: string = 'Susana';
  genero: string = 'femenino';

  invitacionMap = {
    'masculino': 'invitarlo',
    'femenino': 'invitarla',
  }

  //i18nPlural
  clientes: string[] =['Maria','Pedro','Juan','Luis','Manuel'];
  clientesMap = {
    '=0': 'no tenemos cliente esperando.',
    '=1': 'tenemos un cliente esperando.',
    '=2': 'tenemos 2 clientes esperando.',
    '=3': 'tenemos 3 clientes esperando.',
    'other': 'tenemos # clientes esperando.'
  }
 

  cambioPersona(){
    this.nombre = this.nombre !== 'Susana' ? 'Susana' : 'Mario';
    this.genero = this.genero !== 'femenino' ? 'femenino' : 'masculino';
  }

  dropClientes(){
    if(this.clientes.length !== 0 ){
      this.clientes.pop();
    }else {
      this.clientes=['Maria','Pedro','Juan','Luis','Manuel'];
    } 
  }
   //KeyValuePipe
   persona = {
    name: 'anthony',
    lastname: [' rodriguez','Narea'],
    age: ' 21',
    genere: 'Male'
  }

  //JsonPipe
  creatura = [
    { name: 'Vlad Tepes' },
    { race: 'Vampire' },
    { nickname: ['Conde','Dracula'] },
    { age: 580 },
    { genere: 'Vampire Male' }
  ];

  //Async Pipe

  miObservabol = interval(1000);
  valorPromesa =  new Promise((resolve, reject ) => {
    setTimeout(() => {
      resolve('Fin de la Promesa, tenemos la data');
    }, 4500);
  })
}
