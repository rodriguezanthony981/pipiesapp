import { Component } from '@angular/core';

@Component({
  selector: 'app-numeros',
  templateUrl: './numeros.component.html',
  styles: [
  ]
})
export class NumerosComponent {

  ventasNetas: number = 2987456.32167;
  porcentaje : number = 0.48789;

}
