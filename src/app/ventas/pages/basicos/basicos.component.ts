import { Component } from '@angular/core';

@Component({
  selector: 'app-basicos',
  templateUrl: './basicos.component.html',
  styles: [
  ]
})
export class BasicosComponent {
  nombreLower: string = 'anthony';
  nombreUpper: string = 'ANTHONY';
  nombreCompleto: string = 'AntHoNy rodRigUEz';

  fecha: Date = new Date(); // el dia de hoy

}
