import { NgModule } from '@angular/core';

// Modulos
import { MenubarModule } from 'primeng/menubar';

// PrimenG
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ButtonModule } from 'primeng/button';
import { CardModule } from 'primeng/card';
import { RippleModule } from 'primeng/ripple';

import { FieldsetModule } from 'primeng/fieldset';

@NgModule({
  declarations: [],
  exports: [
    BrowserAnimationsModule,
    ButtonModule,
    MenubarModule,
    CardModule,
    RippleModule,
    FieldsetModule
  ]
})
export class PrimengModule { }
